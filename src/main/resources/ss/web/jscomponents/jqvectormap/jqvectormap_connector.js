window.ss_web_jscomponents_jqvectormap_JQVectorMap = function() {

	console.log("Mapper Connector Loading ###############################################");
	
	function frmSubmit(event){
		event = event || window.event;
		debugger;
		var form = this;
	}

	var element = this.getElement();

	var countryColors = {
		'US': '#4E7387',
		'AF': '#333333',
		'RU': '#344B5E'
	};

	var map = $(element).vectorMap({map: 'world_mill_en', 
		onRegionClick: function(event, code) { alert("Code: " + code); },
		regionStyle: { initial: { fill: '#128da7' }, hover: { fill: "#A0D1DC" } },
		series: {
			regions: [{
				values: { 
					'CA': '#3e9d01', 
					'CH': '#4b93c1', 
					'AU': '#c1a14b' 
				},
				attribute: 'fill' 
			}]
		}
	});


	this.onStateChange = function() {
		console.warn("onStateChange fired");
		
//		map.series.regions[0].setValues(countryColors);

	};
};
	

