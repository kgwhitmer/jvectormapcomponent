package ss.web.jscomponents.jqvectormap;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.AbstractJavaScriptComponent;

@StyleSheet("vaadin://jquery-jvectormap-1.2.2.css")
@JavaScript({ "jquery-1.11.0.min.js", "jquery-jvectormap-1.2.2.min.js", "jquery-jvectormap-world-mill-en.js", "jqvectormap_connector.js" })

public class JQVectorMap extends AbstractJavaScriptComponent{

	private static final long serialVersionUID = 1L;
	
	@Override
	public JQVectorMapState getState() {
		return (JQVectorMapState) super.getState();
	}
}
