package ss.web;

import javax.servlet.annotation.WebServlet;

import ss.web.jscomponents.jqvectormap.JQVectorMap;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI
{
	@WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "ss.web.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }
    
    JQVectorMap jqvMap = new JQVectorMap();

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.setSizeFull();
        setContent(mainLayout);
        
        jqvMap.setSizeFull();
        
        mainLayout.addComponent(jqvMap);
    }

}
